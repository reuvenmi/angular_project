// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyDSU5SKStq6Tviaz6kn4iYbkfZthV_hS7w",
    authDomain: "socialnetworkproject-f1977.firebaseapp.com",
    databaseURL: "https://socialnetworkproject-f1977.firebaseio.com",
    projectId: "socialnetworkproject-f1977",
    storageBucket: "socialnetworkproject-f1977.appspot.com",
    messagingSenderId: "732096969218"
  } 
};

