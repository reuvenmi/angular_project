import { HttpClient } from '@angular/common/http';
import { environment } from './../environments/environment';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';

import { RouterModule} from '@angular/router';

import { AppComponent } from './app.component';
import { FormsModule }   from '@angular/forms';
import { NavigationComponent } from './navigation/navigation.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { LoginComponent } from './login/login.component';
import { HttpClientModule } from '@angular/common/http';
import { AuthService } from './services/auth.service';
import { AuthGuardService } from './services/auth-guard.service';
import { RegistrationComponent } from './registration/registration.component';
import * as firebase from 'firebase';
import { UserService } from './services/user.service';
import { MessageComponent } from './message/message.component';
import { MessageService } from './services/message.service';
import { ProfileComponent } from './profile/profile.component';

firebase.initializeApp(environment.firebase);

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    NavigationComponent,
    NotFoundComponent,
    RegistrationComponent,
    MessageComponent,
    ProfileComponent,
    
  ],
  imports: [
    BrowserModule,
    AngularFireModule.initializeApp(environment.firebase, 'Angular Social Network Project'),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    RouterModule.forRoot([
      {path:'', component:MessageComponent, canActivate: [AuthGuardService]},
      {path:'login', component:LoginComponent},      
      {path:'registration', component:RegistrationComponent},
      {path: 'profile/:userId', component: ProfileComponent, canActivate: [AuthGuardService]}, 
      {path:'**', component:NotFoundComponent}
    ]),
    FormsModule,
    HttpClientModule,
  ],
  providers: [
    AuthService,
    AuthGuardService,
    UserService,
    MessageService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
