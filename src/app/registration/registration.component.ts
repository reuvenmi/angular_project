import { Component, OnInit, Injectable } from '@angular/core';
import { User } from '../models/user';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})

@Injectable()
export class RegistrationComponent implements OnInit {
  user: User;
  password : string;
  image: File;
  response: string;
  constructor(private authService: AuthService, private router: Router) {
    this.user = new User();
   }

  ngOnInit() {
  }

  signUpWithMail() {
    this.authService.signUp(this.user, this.password, this.image).then((res) => {
      console.log(res);
      this.router.navigate(['/']);
    }).catch((error) => {
      this.response = error;
    });
  }
  
  handleFileInput(files :any[]) {
    this.image = files[0];
  }
}
