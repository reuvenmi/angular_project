import { Timestamp } from "rxjs";

export class Message {
    author_uuid: string;
    date: number;
    content: Text;
    comments_message_uuid: number[];
    comments: any;
    applause: Map<string, boolean> = new Map();
}
