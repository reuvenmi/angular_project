export class User {
    name: string;
    mail: string;
    gender: string ="";
    imgPath: string;
    birth_date: Date;
    city: string;
    viewers: number = 0;
}
