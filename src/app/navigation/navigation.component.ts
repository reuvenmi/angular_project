import { Observable } from 'rxjs/Observable';
import { Component, OnInit, Input, ChangeDetectorRef, Injectable } from '@angular/core';
import { User } from '../models/user';
import { AuthService } from '../services/auth.service';
import { AngularFireAuth } from 'angularfire2/auth';

@Component({
  selector: 'navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css'],
})

@Injectable()
export class NavigationComponent implements OnInit {
  private authService: AuthService;
  userInfo : User = new User();
  loggedIn: boolean;
  constructor(private _firebaseAuth: AngularFireAuth, authService: AuthService, private cd: ChangeDetectorRef)  {
    this.authService = authService;
    this.authService.isLoggedIn().subscribe((loggedIn) => {
      this.loggedIn = loggedIn;
    });

    this.authService.userInfo.subscribe((userInfo) => {
      this.userInfo = userInfo;
    });
    
  }

  getCurrentUserId() {

    return this.authService.userDetails != null ? this.authService.userDetails.uid : '';
  }

  logout() {
    this.authService.logout();
  }
  
  ngOnInit() {
  
  }



}
