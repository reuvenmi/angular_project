import { Component, OnInit, Input, SimpleChanges, OnDestroy } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { User } from '../models/user';
import { Message } from '../models/message';
import { MessageService } from '../services/message.service';
import { UserService } from '../services/user.service';
import { FormControl } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { AngularFireDatabase } from 'angularfire2/database';

@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.css']
})
export class MessageComponent implements OnInit, OnDestroy {

 
  userInfo: User;
  applauseAudio: any;
  newMessage: Message = new Message();
  comment: Message = new Message();
  messages: any = {};
  messageKeys: string[] = [];
  users: any = {};
  filterByUserId: string;
  applauseSubscriber: any;
  messagesSubscriber: any;
  private applauseUrl : string;
  constructor(private userService: UserService, private db:AngularFireDatabase, private route: ActivatedRoute, private messageService: MessageService, private authService: AuthService) {
    this.authService.userInfo.subscribe((userInfo) => {
      this.userInfo = userInfo;
    });
    this.route.params.subscribe((params) => {
      this.filterByUserId = params['userId'];
      this.messageService.getMessages(this.filterByUserId);
      this.getMessages();
      this.getUsers();
    });
  }

  ngOnInit() {
    this.db.app.storage().ref().child("claps2.mp3").getDownloadURL().then((url) => {
      this.applauseUrl = url;
      this.applauseAudio = new Audio(this.applauseUrl);
      this.applauseAudio.load();
    }).catch((error) => {
      console.log(error);
    });

  }


  public getMessages() {
    if (!this.applauseSubscriber) {
     
      this.applauseSubscriber = this.messageService.applause$.subscribe((applause) => {
        if (applause) {         
          this. applauseAudio.play();
          console.log("play applause");
        }
      });
    }

    if (!this.messagesSubscriber) {
      this.messagesSubscriber = this.messageService.messages$.subscribe((messages) => {
        if (messages) {
          this.messages = messages;
          this.messageKeys = Object.keys(messages);
        } else {
          this.messages = {};
          this.messageKeys = [];
        }
      });
    }
  }

  public getUsers() {
    this.userService.users$.subscribe((users) => {
      if (users) {
        this.users = users;
      }
    });
  }

  getCurrentUserId() {
    return this.authService.userDetails.uid;
  }


  public messageMessage(newMessage: Message) {
    newMessage.date = Date.now();
    newMessage.author_uuid = this.authService.userDetails.uid;
    this.messageService.messageMessage(newMessage).then((data) => {
      this.newMessage = new Message();
    }).catch((error) => {
      console.log(error);
    });
  }

  public messageComment(messageId: string) {
    let comment: Message = new Message();
    comment.content = this.messages[messageId].comment;
    comment.date = Date.now();
    comment.author_uuid = this.authService.userDetails.uid;
    this.messageService.messageComment(messageId, comment).then((data) => {

    }).catch((error) => {
      console.log(error);
    });
  }

  public addApplause(messageId: string) {
    let message = this.messages[messageId];
    this.messageService.applause(messageId, this.authService.userDetails.uid).then((data) => {
    }).catch((error) => {
      console.log(error);
    });
  }

  getJsonValues(json: any) {
    if (json) {
      return Object.values(json);
    }
    return [];
  }

  getJsonKeys(json: any) {
    if (json) {
      return Object.keys(json);
    }
    return [];
  }

  ngOnDestroy(): void {
    this.applauseSubscriber.unsubscribe();
    this.messagesSubscriber.unsubscribe();
  }
}
