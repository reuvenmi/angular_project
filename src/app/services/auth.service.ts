import { Injectable } from '@angular/core';
import { Router } from "@angular/router";
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';
import { Observable } from 'rxjs/Observable';
import { User } from '../models/user';
import { BehaviorSubject, Subject } from 'rxjs';
import { AngularFireDatabase } from 'angularfire2/database';
import { UserService } from './user.service';

@Injectable()
export class AuthService {
  userDetails: firebase.User = null;
  userInfo: Subject<User> = new BehaviorSubject(null);
  private user: Observable<firebase.User>;
  private loggedIn$: Subject<boolean> = new BehaviorSubject(false);
  

  constructor( private _firebaseAuth: AngularFireAuth,private userService:UserService, private router: Router, private db:AngularFireDatabase) {
    this.loggedIn$.asObservable();
    this.user = this._firebaseAuth.authState;
    
    this.user.subscribe(
      (user) => {
        if (user) {
          this.userDetails = user;
          this.loggedIn$.next(true);
          console.log(this.userDetails);
          this.userService.getUserInfo(this.userDetails.uid).on('value', (userFromDb) => {
            this.userInfo.next(userFromDb.val());
          });
        }
        else {
          this.userDetails = null;
          this.loggedIn$.next(false);
        }
      }
    );
  }

  isLoggedIn() {
    return this.loggedIn$.asObservable();
  }

  public signInRegular(email, password) {
    const credential = firebase.auth.EmailAuthProvider.credential(email, password);
    return this._firebaseAuth.auth.signInWithEmailAndPassword(email, password);
  }

  public logout(): any {
    this._firebaseAuth.auth.signOut();
  }

  public updateUserProfile(uuid: string, user: User, image: File) { 
      let storageRequest = this.db.app.storage().ref().child("images/"+uuid+image.name).put(image);
      return storageRequest.on(firebase.storage.TaskEvent.STATE_CHANGED, (snapshot:any) => {
        console.log(snapshot.state);            
      }, (error) => {
        return error;
      }, () => {
        user.imgPath = storageRequest.snapshot.downloadURL;
        return this.db.database.ref().child("users").child(uuid).set(user).then((data) => {
          return "Success";
        }).catch((error) => {
          return error;
        })
      });   
      

  }

  public signUp(user: User, password: string,  image: File) : Promise<any> {
    let promise : Promise<any>;
    return this._firebaseAuth.auth.createUserWithEmailAndPassword(user.mail, password)
    .then((data) => {
      return Promise.resolve(this.updateUserProfile(this._firebaseAuth.auth.currentUser.uid, user, image));
    })
    .catch(function(error) {
      // Handle Errors here.
      var errorCode = error.code;
      var errorMessage = error.message;
      return Promise.reject(errorMessage);
    });

  }
  

  authenticated(): boolean {
    return this.userDetails !== null;
  }

}