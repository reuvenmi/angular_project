import { Injectable } from '@angular/core';
import { Message } from '../models/message';
import { AngularFireDatabase, DatabaseQuery } from 'angularfire2/database';
import { Subject, BehaviorSubject } from 'rxjs';
import { AuthService } from './auth.service';

@Injectable()
export class MessageService {


  filterByUserId: string = null;
  messages$: Subject<Map<string, Message>> = new BehaviorSubject(null);
  messages: any = {};
  applause$: Subject<string> = new BehaviorSubject(null);
  private childChanged: any;
  private childAdded: any;
  messagesRef : DatabaseQuery;
  getMessagesRef(filterByUserId: string = null): DatabaseQuery {
    if (this.messagesRef) {
      this.messagesRef.off('child_added');
      this.messagesRef.off('child_changed');
      this.messagesRef.off('value');
    }
   
    if (filterByUserId) {
      return this.db.database.ref()
        .child("messages")
        .orderByChild('author_uuid')
        .equalTo(filterByUserId)
        .limitToLast(50);
    } else {
      return this.db.database.ref().child("messages").orderByChild('date').limitToLast(50);
    }
  }

  getMessages(filterByUserId: string = null) {
    this.messagesRef = this.getMessagesRef(filterByUserId);
    this.messagesRef.once('value').then((messages) => {
      if (messages) {
        this.messages = messages.val();
        this.messages$.next(this.messages);
      }
    }).catch((error) => {
      console.log(error);
    });

    this.messagesRef.on('child_added', (message) => {
        if (this.messages && message) {
          this.messages[message.key] = {};
          this.messages[message.key] = message.val();
        }

        this.messages$.next(this.messages);
      });
    


    this.messagesRef.on('child_changed', (message) => {
        if (this.messages && message) {
          if (this.authService.userDetails.uid == message.val().author_uuid
            && message.val().applause && Object.keys(message.val().applause).length > (this.messages[message.key].applause ? Object.keys(this.messages[message.key].applause).length : 0)) {
            this.applause$.next(message.key);
          }

          this.messages[message.key] = {};
          this.messages[message.key] = message.val();
        }
        this.applause$.next(null);
        this.messages$.next(this.messages);
      });
    
  }


  constructor(private authService: AuthService, private db: AngularFireDatabase) {
  }

  messageMessage(newMessage: Message) {
    return this.db.database.ref().child("messages").push().set(newMessage);
  }

  messageComment(messageId: string, comment: Message) {
    return this.db.database.ref().child("messages").child(messageId).child("comments").push().set(comment);
  }

  applause(messageId: string, uuid: string) {
    return this.db.database.ref().child("messages").child(messageId).child("applause").child(uuid).set(Date.now());
  }

}
