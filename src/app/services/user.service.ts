import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { Router } from '@angular/router';
import { AngularFireDatabase } from 'angularfire2/database';
import { Subject, BehaviorSubject } from 'rxjs';
import { User } from 'firebase/app';
import { AuthService } from './auth.service';
import { User as UserModel } from '../models/user';

@Injectable()
export class UserService {

  users$: Subject<Map<string, User>> = new BehaviorSubject(null);
  users: any = {};

  getUserInfo(uuid: any): any {
    return this.db.database.ref("users/" + uuid);
  }

  getUsersRef(): any {
    return this.db.database.ref().child("users");
  }

  getUsers() {
    this.getUsersRef().once('value').then((users) => {
      if (users) {
        this.users = users.val();
        this.users$.next(this.users);
      }
    }).catch((error) => {
      console.log(error);
    });

    this.getUsersRef().on('child_added', (user) => {
      this.users[user.key] = {};
      this.users[user.key] = user.val();
      this.users$.next(this.users);
    });

    this.getUsersRef().on('child_changed', (user) => {
      this.users[user.key] = user.val();
      this.users$.next(this.users);
    });
  }

  saveUser(profileUuid: string, user: UserModel) {
      return this.db.database.ref().child("users").child(profileUuid).set(user);
  }

  constructor(private _firebaseAuth: AngularFireAuth, private router: Router, private db: AngularFireDatabase) {
    this.getUsers();
  }
}
